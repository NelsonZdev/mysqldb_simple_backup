#!/usr/bin/python3
import os
import time
import pipes

BACKUP_PATH = "../" + "backup"
SQL_BACKUP_MANAGER_FOLDER = "/usr/bin/"  # Default SQL Instalation folder
backupManagerFolder = ""  # Alternative folder


class SQLEngine:

    __DB_HOST = "127.0.0.1"
    __DB_USER = "root"
    __DB_PASS = "root"
    DB_NAME = "web_DB"

    def __init__(self):
        pass

    def MySql(self, todayPath):
        return sqlFolder() + "mysqldump -h " + \
            self.__DB_HOST + " -u " + \
            self.__DB_USER + " -p" + \
            self.__DB_PASS + " " + \
            self.DB_NAME + " > " + \
            pipes.quote(todayPath) + "/" + self.DB_NAME + ".sql"


# Folder used by sqlBackupManager
def sqlFolder():
    try:
        os.stat(SQL_BACKUP_MANAGER_FOLDER + "mysqldump")
        return SQL_BACKUP_MANAGER_FOLDER
    except FileNotFoundError:
        print("Default folder not found; Using alternative folder")
        return "/opt/lampp/bin/"


def newPath(name):
    try:
        os.stat(name)
    except FileNotFoundError:
        os.mkdir(name)


def main():
    # BackupFolder
    newPath(BACKUP_PATH)
    # Class instance
    sqlEngine = SQLEngine()
    # New folder
    actualTime = time.strftime('%Y%m%d-%H%M%S')  # Actual date
    timePath = BACKUP_PATH + '/' + actualTime  # Path + actual date
    newPath(timePath)
    # Execute backup command
    os.system(sqlEngine.MySql(timePath))  # SQL Engine command (MYSQL)
    # Final message
    try:
        os.stat(timePath + "/" + sqlEngine.DB_NAME + ".sql")
        os.system("echo Success!!")
    except FileNotFoundError:
        os.system("echo Failed!!")

    # Dev - Delete in production
    os.system("echo Altering Database...")
    bashScript = sqlFolder() + "mysql -h " + \
        "127.0.0.1" + " -u " + \
        "root" + " -p" + \
        "root" + " " + \
        "web_DB_test" + " > " + \
        "altertable.sql"
    os.system(bashScript)


if __name__ == "__main__":
    main()
